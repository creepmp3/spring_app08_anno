package spring_app8_anno;

import org.springframework.beans.factory.annotation.Autowired;

public class GreetingImpl implements Greeting{
    String msg;
    
    // @Autowired : NowTime nt에 값을 Spring이 자동으로 넣어줌
    @Autowired
    NowTime nt;
    
    public GreetingImpl(){
    }
    
    public GreetingImpl(String msg, NowTime nt){
        super();
        this.msg = msg;
        this.nt = nt;
    }
    
    public void setMsg(String msg){
        this.msg = msg;
    }

    public void setNt(NowTime nt){
        this.nt = nt;
    }
    
    @Override
    public void printMsg() {
        System.out.println("지금시각 : " + nt.getTime() + msg);
    }
    
}
